<?php
// $Id$

/**
 * @file
 * Template override
 */
?>
<?php if ($block->block_schedule_flag) { ?>
  <div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">

    <?php if (!empty($block->subject)): ?>
      <h2><?php print $block->subject ?></h2>
    <?php endif;?>

    <div class="content"><?php print $block->content ?></div>
  </div>
<?php } ?>