// We need to set default values during "Edit" operation of block
Drupal.behaviors.blockSchedule = function() {
  jQuery('#edit-startdate-datepicker-popup-0').val(Drupal.settings.block_schedule.startdate);
  jQuery('#edit-enddate-datepicker-popup-0').val(Drupal.settings.block_schedule.enddate);
  jQuery('#edit-startdate-timeEntry-popup-1').val(Drupal.settings.block_schedule.starttime);
  jQuery('#edit-enddate-timeEntry-popup-1').val(Drupal.settings.block_schedule.endtime);
}