== Description ==
This module allows you to schedule any blocks displaying between certain dates. Say for example, you want your "Christmas Gift Block" get displayed only between 25th of December to 31th December this is where this module comes in use.

While adding/editing any block there will be provided Start-Date and End-Date text boxes. Just enter proper dates, and you are ready to go!

== Installation ==

1. Enable the module.

2. While adding/editing block from admin side, you will be prompted for Stand Date And End Date.
